# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vrsa_dev/vs_update/vistudio/map.jinja" import vs_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}
{% set isPathExist = salt['cmd.run']('if exist '~ vs_settings.config.installer ~' \(echo true\) else \(echo false\)') %}

{% if OSFAMILY == "Windows" %}

{% for process in vs_settings.config.processes %}
stop_process_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL
{% endfor %}

{% if isPathExist %}
install_visual_studio:
  cmd.run:
    - name: {{ vs_settings.config.installer }} --installPath {{ vs_settings.config.install_path }} --add Microsoft.VisualStudio.Workload.CoreEditor --quiet --norestart
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass"
    - require:
      - stop_process_devenv

{% else %}
skip_vs_installation:
  test.fail_without_changes:
    - name: "{{ vs_settings.config.installer }} does not exist or inaccessible!"
    - failhard: true
{% endif %}

{% else %}
skip_non_windows:
  test.show_notification:
    - text: "Visual Studio update state is not supported on {{ OSFAMILY }}."
{% endif %}